﻿namespace iks_oks
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_f = new System.Windows.Forms.Label();
            this.lbl_s = new System.Windows.Forms.Label();
            this.txtB_Prvi = new System.Windows.Forms.TextBox();
            this.txtB_Drugi = new System.Windows.Forms.TextBox();
            this.btnPlay = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.lblPlayer_1 = new System.Windows.Forms.Label();
            this.lbl_Rez1 = new System.Windows.Forms.Label();
            this.lbl_Rez2 = new System.Windows.Forms.Label();
            this.lblPlayer_2 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_Potez = new System.Windows.Forms.Label();
            this.btnNew = new System.Windows.Forms.Button();
            this.Izlaz = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_f
            // 
            this.lbl_f.AutoSize = true;
            this.lbl_f.Location = new System.Drawing.Point(147, 137);
            this.lbl_f.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_f.Name = "lbl_f";
            this.lbl_f.Size = new System.Drawing.Size(48, 13);
            this.lbl_f.TabIndex = 0;
            this.lbl_f.Text = "Player 1:";
            // 
            // lbl_s
            // 
            this.lbl_s.AutoSize = true;
            this.lbl_s.Location = new System.Drawing.Point(278, 137);
            this.lbl_s.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_s.Name = "lbl_s";
            this.lbl_s.Size = new System.Drawing.Size(48, 13);
            this.lbl_s.TabIndex = 1;
            this.lbl_s.Text = "Player 2:";
            // 
            // txtB_Prvi
            // 
            this.txtB_Prvi.Location = new System.Drawing.Point(110, 167);
            this.txtB_Prvi.Margin = new System.Windows.Forms.Padding(2);
            this.txtB_Prvi.Name = "txtB_Prvi";
            this.txtB_Prvi.Size = new System.Drawing.Size(119, 20);
            this.txtB_Prvi.TabIndex = 2;
            // 
            // txtB_Drugi
            // 
            this.txtB_Drugi.Location = new System.Drawing.Point(254, 167);
            this.txtB_Drugi.Margin = new System.Windows.Forms.Padding(2);
            this.txtB_Drugi.Name = "txtB_Drugi";
            this.txtB_Drugi.Size = new System.Drawing.Size(119, 20);
            this.txtB_Drugi.TabIndex = 3;
            // 
            // btnPlay
            // 
            this.btnPlay.Location = new System.Drawing.Point(182, 208);
            this.btnPlay.Margin = new System.Windows.Forms.Padding(2);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(100, 32);
            this.btnPlay.TabIndex = 4;
            this.btnPlay.Text = "play";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(101, 56);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(101, 107);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(197, 56);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(101, 107);
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseUp);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.White;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Location = new System.Drawing.Point(289, 56);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(101, 107);
            this.pictureBox3.TabIndex = 7;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox3_MouseUp);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.White;
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4.Location = new System.Drawing.Point(101, 154);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(101, 107);
            this.pictureBox4.TabIndex = 10;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox4_MouseUp);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackColor = System.Drawing.Color.White;
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox5.Location = new System.Drawing.Point(197, 154);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(101, 107);
            this.pictureBox5.TabIndex = 9;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox5_MouseUp);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.White;
            this.pictureBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox6.Location = new System.Drawing.Point(289, 154);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(101, 107);
            this.pictureBox6.TabIndex = 8;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox6_MouseUp);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackColor = System.Drawing.Color.White;
            this.pictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox7.Location = new System.Drawing.Point(101, 261);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(101, 107);
            this.pictureBox7.TabIndex = 13;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox7_MouseUp);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.Color.White;
            this.pictureBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox8.Location = new System.Drawing.Point(197, 261);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(101, 107);
            this.pictureBox8.TabIndex = 12;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox8_MouseUp);
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.White;
            this.pictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox9.Location = new System.Drawing.Point(289, 261);
            this.pictureBox9.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(101, 107);
            this.pictureBox9.TabIndex = 11;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox9_MouseUp);
            // 
            // lblPlayer_1
            // 
            this.lblPlayer_1.AutoSize = true;
            this.lblPlayer_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPlayer_1.Location = new System.Drawing.Point(13, 366);
            this.lblPlayer_1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPlayer_1.Name = "lblPlayer_1";
            this.lblPlayer_1.Size = new System.Drawing.Size(45, 13);
            this.lblPlayer_1.TabIndex = 14;
            this.lblPlayer_1.Text = "Player 1";
            // 
            // lbl_Rez1
            // 
            this.lbl_Rez1.AutoSize = true;
            this.lbl_Rez1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_Rez1.Location = new System.Drawing.Point(12, 393);
            this.lbl_Rez1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_Rez1.Name = "lbl_Rez1";
            this.lbl_Rez1.Size = new System.Drawing.Size(14, 20);
            this.lbl_Rez1.TabIndex = 15;
            this.lbl_Rez1.Text = "-";
            // 
            // lbl_Rez2
            // 
            this.lbl_Rez2.AutoSize = true;
            this.lbl_Rez2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_Rez2.Location = new System.Drawing.Point(409, 393);
            this.lbl_Rez2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_Rez2.Name = "lbl_Rez2";
            this.lbl_Rez2.Size = new System.Drawing.Size(14, 20);
            this.lbl_Rez2.TabIndex = 16;
            this.lbl_Rez2.Text = "-";
            // 
            // lblPlayer_2
            // 
            this.lblPlayer_2.AutoSize = true;
            this.lblPlayer_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPlayer_2.Location = new System.Drawing.Point(410, 366);
            this.lblPlayer_2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPlayer_2.Name = "lblPlayer_2";
            this.lblPlayer_2.Size = new System.Drawing.Size(45, 13);
            this.lblPlayer_2.TabIndex = 17;
            this.lblPlayer_2.Text = "Player 2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(187, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 18);
            this.label2.TabIndex = 18;
            this.label2.Text = "Na Potezu:";
            // 
            // lbl_Potez
            // 
            this.lbl_Potez.AutoSize = true;
            this.lbl_Potez.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_Potez.Location = new System.Drawing.Point(269, 18);
            this.lbl_Potez.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbl_Potez.Name = "lbl_Potez";
            this.lbl_Potez.Size = new System.Drawing.Size(13, 18);
            this.lbl_Potez.TabIndex = 19;
            this.lbl_Potez.Text = "-";
            // 
            // btnNew
            // 
            this.btnNew.Location = new System.Drawing.Point(16, 466);
            this.btnNew.Margin = new System.Windows.Forms.Padding(2);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(84, 28);
            this.btnNew.TabIndex = 20;
            this.btnNew.Text = "Nova igra";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // Izlaz
            // 
            this.Izlaz.Location = new System.Drawing.Point(397, 470);
            this.Izlaz.Name = "Izlaz";
            this.Izlaz.Size = new System.Drawing.Size(75, 23);
            this.Izlaz.TabIndex = 21;
            this.Izlaz.Text = "Izlaz";
            this.Izlaz.UseVisualStyleBackColor = true;
            this.Izlaz.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 505);
            this.Controls.Add(this.Izlaz);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.lbl_Potez);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblPlayer_2);
            this.Controls.Add(this.lbl_Rez2);
            this.Controls.Add(this.lbl_Rez1);
            this.Controls.Add(this.lblPlayer_1);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.txtB_Drugi);
            this.Controls.Add(this.txtB_Prvi);
            this.Controls.Add(this.lbl_s);
            this.Controls.Add(this.lbl_f);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Iks-Oks";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_f;
        private System.Windows.Forms.Label lbl_s;
        private System.Windows.Forms.TextBox txtB_Prvi;
        private System.Windows.Forms.TextBox txtB_Drugi;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label lblPlayer_1;
        private System.Windows.Forms.Label lbl_Rez1;
        private System.Windows.Forms.Label lbl_Rez2;
        private System.Windows.Forms.Label lblPlayer_2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_Potez;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button Izlaz;
    }
}

