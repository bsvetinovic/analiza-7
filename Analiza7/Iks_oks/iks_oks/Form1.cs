﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iks_oks
{
    public partial class Form1 : Form
    {
        Graphics g;
        Pen p = new Pen(Color.Green, 5);

        string Player_1, Player_2;
        int Player_potez = 1, rez_prvog = 0, rez_drugog = 0;
        int[,] iks_oks = new int[3, 3] {{0,0,0},{0,0,0},{0,0,0}};

        class Iks
        {
            PointF x1;
            PointF x2;
            PointF y1;
            PointF y2;

            public Iks()
            {
                x1 = new PointF(20, 20);
                y1 = new PointF(60, 60);
                x2 = new PointF(60, 20);
                y2 = new PointF(20, 60);

            }
            public void draw(Graphics g, Pen p)
            {
                g.DrawLine(p, x1,y1);
                g.DrawLine(p, x2, y2);
            }
        }

        class Circle
        {
            int r;
            public Circle()
            {
                r = 50;
            }
            public void draw(Graphics g, Pen p, int x, int y)
            {
                g.DrawEllipse(p, x, y, r, r);
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            switchToLogInterface();
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            g = pictureBox1.CreateGraphics();

            if (iks_oks[0,0] == 0 && Player_potez == 1)
            {
                p.Color = Color.Red;
                Iks x = new Iks();
                x.draw(g, p);
                Player_potez = 2;
                iks_oks[0, 0] = 1;
                lbl_Potez.Text = Player_2;
            }
            if (iks_oks[0, 0] == 0 && Player_potez == 2)
            {
                p.Color = Color.Green;
                Circle c = new Circle();
                c.draw(g, p, 15, 15);
                Player_potez = 1;
                iks_oks[0, 0] = 2;
                lbl_Potez.Text = Player_1;
            }

            if (Provjera_pobjede(1))
            {;
                Provjera_Win(1);
            }
            else if (Provjera_pobjede(2))
            {
                Provjera_Win(2);
            }
            else if(Provjera_pobjede(1) == false && Provjera_pobjede(2) == false && check_field_num() == 9)
            {
                Provjera_X();
            }
        }

        private void pictureBox2_MouseUp(object sender, MouseEventArgs e)
        {
            g = pictureBox2.CreateGraphics();

            if (iks_oks[0, 1] == 0 && Player_potez == 1)
            {
                p.Color = Color.Red;
                Iks x = new Iks();
                x.draw(g, p);
                Player_potez = 2;
                iks_oks[0, 1] = 1;
                lbl_Potez.Text = Player_2;
            }
            if (iks_oks[0, 1] == 0 && Player_potez == 2)
            {
                p.Color = Color.Green;
                Circle c = new Circle();
                c.draw(g, p, 15, 15);
                Player_potez = 1;
                iks_oks[0, 1] = 2;
                lbl_Potez.Text = Player_1;
            }

            if (Provjera_pobjede(1))
            {
                Provjera_Win(1);
            }
            else if (Provjera_pobjede(2))
            {
                Provjera_Win(2);
            }
            else if (Provjera_pobjede(1) == false && Provjera_pobjede(2) == false && check_field_num() == 9)
            {
                Provjera_X();
            }
        }

        private void pictureBox3_MouseUp(object sender, MouseEventArgs e)
        {
            g = pictureBox3.CreateGraphics();

            if (iks_oks[0, 2] == 0 && Player_potez == 1)
            {
                p.Color = Color.Red;
                Iks x = new Iks();
                x.draw(g, p);
                Player_potez = 2;
                iks_oks[0, 2] = 1;
                lbl_Potez.Text = Player_2;
            }
            if (iks_oks[0, 2] == 0 && Player_potez == 2)
            {
                p.Color = Color.Green;
                Circle c = new Circle();
                c.draw(g, p, 15, 15);
                Player_potez = 1;
                iks_oks[0, 2] = 2;
                lbl_Potez.Text = Player_1;
            }

            if (Provjera_pobjede(1))
            {
                Provjera_Win(1);
            }
            else if (Provjera_pobjede(2))
            {
                Provjera_Win(2);
            }
            else if (Provjera_pobjede(1) == false && Provjera_pobjede(2) == false && check_field_num() == 9)
            {
                Provjera_X();
            }
        }

        private void pictureBox4_MouseUp(object sender, MouseEventArgs e)
        {
            g = pictureBox4.CreateGraphics();

            if (iks_oks[1, 0] == 0 && Player_potez == 1)
            {
                p.Color = Color.Red;
                Iks x = new Iks();
                x.draw(g, p);
                Player_potez = 2;
                iks_oks[1, 0] = 1;
                lbl_Potez.Text = Player_2;
            }
            if (iks_oks[1, 0] == 0 && Player_potez == 2)
            {
                p.Color = Color.Green;
                Circle c = new Circle();
                c.draw(g, p, 15, 15);
                Player_potez = 1;
                iks_oks[1, 0] = 2;
                lbl_Potez.Text = Player_1;
            }

            if (Provjera_pobjede(1))
            {
                Provjera_Win(1);
            }
            else if (Provjera_pobjede(2))
            {
                Provjera_Win(2);
            }
            else if (Provjera_pobjede(1) == false && Provjera_pobjede(2) == false && check_field_num() == 9)
            {
                Provjera_X();
            }
        }

        private void pictureBox5_MouseUp(object sender, MouseEventArgs e)
        {
            g = pictureBox5.CreateGraphics();

            if (iks_oks[1, 1] == 0 && Player_potez == 1)
            {
                p.Color = Color.Red;
                Iks x = new Iks();
                x.draw(g, p);
                Player_potez = 2;
                iks_oks[1, 1] = 1;
                lbl_Potez.Text = Player_2;
            }
            if (iks_oks[1, 1] == 0 && Player_potez == 2)
            {
                p.Color = Color.Green;
                Circle c = new Circle();
                c.draw(g, p, 15, 15);
                Player_potez = 1;
                iks_oks[1, 1] = 2;
                lbl_Potez.Text = Player_1;
            }

            if (Provjera_pobjede(1))
            {
                Provjera_Win(1);
            }
            else if (Provjera_pobjede(2))
            {
                Provjera_Win(2);
            }
            else if (Provjera_pobjede(1) == false && Provjera_pobjede(2) == false && check_field_num() == 9)
            {
                Provjera_X();
            }
        }

        private void pictureBox6_MouseUp(object sender, MouseEventArgs e)
        {
            g = pictureBox6.CreateGraphics();

            if (iks_oks[1, 2] == 0 && Player_potez == 1)
            {
                p.Color = Color.Red;
                Iks x = new Iks();
                x.draw(g, p);
                Player_potez = 2;
                iks_oks[1, 2] = 1;
                lbl_Potez.Text = Player_2;
            }
            if (iks_oks[1, 2] == 0 && Player_potez == 2)
            {
                p.Color = Color.Green;
                Circle c = new Circle();
                c.draw(g, p, 15, 15);
                Player_potez = 1;
                iks_oks[1, 2] = 2;
                lbl_Potez.Text = Player_1;
            }

            if (Provjera_pobjede(1))
            {
                Provjera_Win(1);
            }
            else if (Provjera_pobjede(2))
            {
                Provjera_Win(2);
            }
            else if (Provjera_pobjede(1) == false && Provjera_pobjede(2) == false && check_field_num() == 9)
            {
                Provjera_X();
            }
        }

        private void pictureBox7_MouseUp(object sender, MouseEventArgs e)
        {
            g = pictureBox7.CreateGraphics();

            if (iks_oks[2, 0] == 0 && Player_potez == 1)
            {
                p.Color = Color.Red;
                Iks x = new Iks();
                x.draw(g, p);
                Player_potez = 2;
                iks_oks[2, 0] = 1;
                lbl_Potez.Text = Player_2;
            }
            if (iks_oks[2, 0] == 0 && Player_potez == 2)
            {
                p.Color = Color.Green;
                Circle c = new Circle();
                c.draw(g, p, 15, 15);
                Player_potez = 1;
                iks_oks[2, 0] = 2;
                lbl_Potez.Text = Player_1;
            }

            if (Provjera_pobjede(1))
            {
                Provjera_Win(1);
            }
            else if (Provjera_pobjede(2))
            {
                Provjera_Win(2);
            }
            else if (Provjera_pobjede(1) == false && Provjera_pobjede(2) == false && check_field_num() == 9)
            {
                Provjera_X();
            }
        }

        private void pictureBox8_MouseUp(object sender, MouseEventArgs e)
        {
            g = pictureBox8.CreateGraphics();

            if (iks_oks[2, 1] == 0 && Player_potez == 1)
            {
                p.Color = Color.Red;
                Iks x = new Iks();
                x.draw(g, p);
                Player_potez = 2;
                iks_oks[2, 1] = 1;
                lbl_Potez.Text = Player_2;
            }
            if (iks_oks[2, 1] == 0 && Player_potez == 2)
            {
                p.Color = Color.Green;
                Circle c = new Circle();
                c.draw(g, p, 15, 15);
                Player_potez = 1;
                iks_oks[2, 1] = 2;
                lbl_Potez.Text = Player_1;
            }

            if (Provjera_pobjede(1))
            {
                Provjera_Win(1);
            }
            else if (Provjera_pobjede(2))
            {
                Provjera_Win(2);
            }
            else if (Provjera_pobjede(1) == false && Provjera_pobjede(2) == false && check_field_num() == 9)
            {
                Provjera_X();
            }
        }

        private void pictureBox9_MouseUp(object sender, MouseEventArgs e)
        {
            g = pictureBox9.CreateGraphics();

            if (iks_oks[2, 2] == 0 && Player_potez == 1)
            {
                p.Color = Color.Red;
                Iks x = new Iks();
                x.draw(g, p);
                Player_potez = 2;
                iks_oks[2, 2] = 1;
                lbl_Potez.Text = Player_2;
            }
            if (iks_oks[2, 2] == 0 && Player_potez == 2)
            {
                p.Color = Color.Green;
                Circle c = new Circle();
                c.draw(g, p, 15, 15);
                Player_potez = 1;
                iks_oks[2, 2] = 2;
                lbl_Potez.Text = Player_1;
            }

            if (Provjera_pobjede(1))
            {
                Provjera_Win(1);
            }
            else if (Provjera_pobjede(2))
            {
                Provjera_Win(2);
            }
            else if (Provjera_pobjede(1) == false && Provjera_pobjede(2) == false && check_field_num() == 9)
            {
                Provjera_X();
            }
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            Player_1 = txtB_Prvi.Text;
            Player_2 = txtB_Drugi.Text;
            if (Player_2 == "")
            {
                Player_2 = "Player 2";
            }
            if (Player_1 == "")
            {
                Player_1 = "Player 1";
            }
            lblPlayer_1.Text = Player_1;
            lblPlayer_2.Text = Player_2;
            lbl_Rez1.Text = rez_prvog.ToString();
            lbl_Rez2.Text = rez_prvog.ToString();
            switchToGameInterface();
            txtB_Prvi.Clear();
            txtB_Drugi.Clear();
            lbl_Potez.Text = Player_1;
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            switchToLogInterface();
            matrix_rst();
            Player_potez = 1;
            rez_prvog = 0;
            rez_drugog = 0;
            matrix_rst();
            BrisanjePolja();
        }

        void matrix_rst(){
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    iks_oks[i, j] = 0;
                }
            }
        }

        int check_field_num()
        {
            int filled = 0;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if(iks_oks[i,j] != 0)
                    {
                        filled++;
                    }
                }
            }
            return filled;
        }
        void Provjera_X()
        {
            MessageBox.Show("Nerješeno!");
            BrisanjePolja();
            matrix_rst();

            if (Player_potez == 1)
            {
                Player_potez = 2;
            }
            else
            {
                Player_potez = 1;
            }

        }

        bool Provjera_pobjede(int lik)
        {
            if(iks_oks[0,0] == lik && iks_oks[0, 1] == lik && iks_oks[0, 2] == lik)
            {
                return true; 
            }
            if (iks_oks[1, 0] == lik && iks_oks[1, 1] == lik && iks_oks[1, 2] == lik)
            {
                return true;
            }
            if (iks_oks[2, 0] == lik && iks_oks[2, 1] == lik && iks_oks[2, 2] == lik)
            {
                return true;
            }
            if (iks_oks[0, 0] == lik && iks_oks[1, 0] == lik && iks_oks[2, 0] == lik)
            {
                return true;
            }
            if (iks_oks[0, 1] == lik && iks_oks[1, 1] == lik && iks_oks[2, 1] == lik)
            {
                return true;
            }
            if (iks_oks[0, 2] == lik && iks_oks[1, 2] == lik && iks_oks[2, 2] == lik)
            {
                return true;
            }
            if (iks_oks[0, 0] == lik && iks_oks[1, 1] == lik && iks_oks[2, 2] == lik)
            {
                return true;
            }
            if (iks_oks[0, 2] == lik && iks_oks[1, 1] == lik && iks_oks[2, 0] == lik)
            {
                return true;
            }
            return false;
        }

        

        void Provjera_Win(int igrac)
        {
            if(igrac == 1)
            {
                Player_potez = 2;
                rez_prvog++;
                MessageBox.Show("Pobjednik je " + Player_1 + " X");
                lbl_Rez1.Text = (rez_prvog).ToString();
            }
            if(igrac == 2)
            {
                Player_potez = 1;
                rez_drugog++;
                MessageBox.Show("Pobjednik je " + Player_2 + " O");
                lbl_Rez2.Text = (rez_drugog).ToString();
            }
            BrisanjePolja();
            matrix_rst();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        void BrisanjePolja()
        {
            pictureBox1.Image = null;
            pictureBox2.Image = null;
            pictureBox3.Image = null;
            pictureBox4.Image = null;
            pictureBox5.Image = null;
            pictureBox6.Image = null;
            pictureBox7.Image = null;
            pictureBox8.Image = null;
            pictureBox9.Image = null;
        }

        void switchToLogInterface()
        {
            pictureBox1.Hide();
            pictureBox1.Enabled = false;
            pictureBox2.Hide();
            pictureBox2.Enabled = false;
            pictureBox3.Hide();
            pictureBox3.Enabled = false;
            pictureBox4.Hide();
            pictureBox4.Enabled = false;
            pictureBox5.Hide();
            pictureBox5.Enabled = false;
            pictureBox6.Hide();
            pictureBox6.Enabled = false;
            pictureBox7.Hide();
            pictureBox7.Enabled = false;
            pictureBox8.Hide();
            pictureBox8.Enabled = false;
            pictureBox9.Hide();
            pictureBox9.Enabled = false;
            btnNew.Hide();
            btnNew.Enabled = false;
            lblPlayer_1.Hide();
            lblPlayer_2.Hide();
            lbl_Potez.Hide();
            lbl_Rez1.Hide();
            lbl_Rez2.Hide();
            label2.Hide();
            btnPlay.Show();
            btnPlay.Enabled = true;
            lbl_f.Show();
            lbl_s.Show();
            txtB_Prvi.Show();
            txtB_Prvi.Enabled = true;
            txtB_Drugi.Show();
            txtB_Drugi.Enabled = true;
        }

        void switchToGameInterface()
        {
            pictureBox1.Show();
            pictureBox1.Enabled = true;
            pictureBox2.Show();
            pictureBox2.Enabled = true;
            pictureBox3.Show();
            pictureBox3.Enabled = true;
            pictureBox4.Show();
            pictureBox4.Enabled = true;
            pictureBox5.Show();
            pictureBox5.Enabled = true;
            pictureBox6.Show();
            pictureBox6.Enabled = true;
            pictureBox7.Show();
            pictureBox7.Enabled = true;
            pictureBox8.Show();
            pictureBox8.Enabled = true;
            pictureBox9.Show();
            pictureBox9.Enabled = true;
            btnNew.Show();
            btnNew.Enabled = true;
            lblPlayer_1.Show();
            lblPlayer_2.Show();
            lbl_Potez.Show();
            lbl_Rez1.Show();
            lbl_Rez2.Show();
            label2.Show();
            btnPlay.Hide();
            btnPlay.Enabled = false;
            lbl_f.Hide();
            lbl_s.Hide();
            txtB_Prvi.Hide();
            txtB_Prvi.Enabled = false;
            txtB_Drugi.Hide();
            txtB_Drugi.Enabled = false;
        }
    }
}
